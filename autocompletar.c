#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct n {
	struct n *subtrie[26];
	int ehFim;
} trie;

void inserir(trie *ele, char *palavra) {

	trie *raiz = ele;

	int len = strlen(palavra);		
		
	for (int i = 0; i < len; i++) {

		trie *atual = ele->subtrie[palavra[i] % 97];

		if (atual == NULL) {
			atual = malloc (sizeof (trie));
			atual->ehFim = 0;
			for(int i = 0; i < 26; i++) atual->subtrie[i] = NULL;
		}
	
		if(i + 1 == len) atual->ehFim = 1;

		ele->subtrie[palavra[i] % 97] = atual;
		ele = ele->subtrie[palavra[i] % 97];
	}

	ele = raiz;
}

char *acrescentar (char *fatia, char parte) {

	char *str = malloc (sizeof (char) * (strlen (fatia) + 2));
	
	int i = 0;
	while (fatia[i] != '\0') str[i] = fatia[i++];

	str[i++] = parte;
	str[i] = '\0';

	return str;
}

void imprimir(trie *ele, char *fatia) {
	
	if (ele == NULL) return;
	
	if (ele->ehFim)
		printf("->%s\n", fatia);
	
	
	for (int i = 0; i < 26; i++)
		if(ele->subtrie[i] != NULL)
			imprimir(ele->subtrie[i], acrescentar (fatia, i + 97));
		
}

void autocompletar(trie *ele, char *prefixo) {

	int len = strlen(prefixo);
	
	for (int i = 0; i < len; i++) ele = ele->subtrie[prefixo[i] % 97];
	
	imprimir(ele, prefixo);
}

int main() {

	trie n;
	n.ehFim = 0;
	
	for (int i = 0; i < 26; i++) n.subtrie[i] = NULL;
	
	FILE *fp = fopen ("dicionario.txt", "r");

	char str[25];
	
	while (!feof (fp)) {
		fscanf (fp, "%s", str);
		inserir (&n, str);
	}
	
	fclose (fp);

	char input[100];

	while (1) { 
		system("cls");
		printf ("crtl+c caso queira sair...\n\nDigite uma parte de palavra aqui >> ");
		scanf ("%s", input);
		printf ("\nPossiveis palavras:\n");
		autocompletar (&n, input);
		system("pause");
	}

	return 0;
}